local sensorInfo = {
	name = "BearsUnloaded",
	desc = "Returns true if all the Bear transporters in the given units are free.",
	author = "Matyas",
	date = "2022-05-11",
	license = "notAlicense",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description Returns ID of the leader of the group.
return function()
	if #units > 0 then
		for i = 1, #units do
			if Spring.ValidUnitID(units[i]) then
				local defID = Spring.GetUnitDefID(units[i])

				if UnitDefs[defID].name == "armthovr" and #Spring.GetUnitIsTransporting(units[i]) ~= 0 then
					return false
				end
			end
		end
	end

	return true
end