local sensorInfo = {
	name = "FastAndDumbConquer",
	desc = "Sends units on one of the given points (one unit at a time from the closest to the furthest point - assumes all units are roughly at the same place at the beginning - unused units stay at place). Returns the uncovered points.",
	author = "Matyas",
	date = "2022-05-11",
	license = "notAlicense",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description Sends units on one of the given points (one unit at a time from the closest to the furthest point - assumes all units are roughly at the same place at the beginning - unused units stay at place). Returns the uncovered points.
return function(pointsToConquer)
	local pointsAssigned = {}
	for i = 1, #pointsToConquer do
		pointsAssigned[i] = false
		pointsToConquer[i] = Vec3(pointsToConquer[i].x, Spring.GetGroundHeight(pointsToConquer[i].x, pointsToConquer[i].z), pointsToConquer[i].z)
	end

	if #units > 0 then
		for i = 1, #units do
			if Spring.ValidUnitID(units[i]) then
				local x, _, z = Spring.GetUnitPosition(units[i])
				local unitPos = Vec3(x, _, z)

				local closestPointIndex = nil
				local closestPointDistance = math.huge
				for j = 1, #pointsToConquer do
					local distance = unitPos:Distance( pointsToConquer[j] )
					if not pointsAssigned[j] and distance < closestPointDistance then
						closestPointIndex = j
						closestPointDistance = distance
					end
				end

				if closestPointIndex == nil then -- all points assigned
					break
				end

				Spring.GiveOrderToUnit(units[i], CMD.FIGHT, pointsToConquer[closestPointIndex]:AsSpringVector(), {})
				pointsAssigned[closestPointIndex] = true
			end
		end
	end

	local j = 1
	local uncoveredPoints = {}
	for i = 1, #pointsToConquer do
		if pointsAssigned[i] == false then
			uncoveredPoints[j] = pointsToConquer[i]
			j = j + 1
		end
	end

	return uncoveredPoints
end
