local sensorInfo = {
	name = "PositionOfLeader",
	desc = "Returns group for formation.moveCustomGroup of at most 36 units for wedge formation.",
	author = "Matyas",
	date = "2022-05-04",
	license = "notAlicense",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description Returns group for formation.moveCustomGroup of at most 36 units for wedge formation.
return function()
	if #units > 0 then
		local counter = 1
		local group = {}
		for i = 1, #units do
			if Spring.ValidUnitID(units[i]) then
				local defID = Spring.GetUnitDefID(units[i])

				if UnitDefs[defID].name ~= "armbcom" and counter <= 36 then
					group[units[i]] = counter
					counter = counter + 1
				elseif counter > 36 then
					break
				end
			end
		end

		return group
	end

	return nil
end