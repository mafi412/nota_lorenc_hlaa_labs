local sensorInfo = {
	name = "HighGrounds",
	desc = "Returns table of all the highest points on the map (for every plateau returns one point (roughly its center)).",
	author = "Matyas",
	date = "2022-05-10",
	license = "notAlicense",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description Returns table of all the highest points on the map (for every plateau returns one point (roughly its center)).
return function()
	local mapSize = { x = Game.mapSizeX, z = Game.mapSizeZ }
	local step = 64

	local _, maxHeight = Spring.GetGroundExtremes()

	local maxHeightMask = {}
	for x = 0, mapSize.x, step do
		for z = 0, mapSize.z, step do
			if Spring.GetGroundHeight(x, z) == maxHeight then
				maxHeightMask[x .. "," .. z] = 1
			end
		end
	end

	local plateaus = {}
	local pointsToPlateaus = {}
	local newPlateauCounter = 1
	for x = 0, mapSize.x, step do
		for z = 0, mapSize.z, step do
			if maxHeightMask[x .. "," .. z] == 1 then
				if pointsToPlateaus[x .. "," .. z] == nil then
					pointsToPlateaus[x .. "," .. z] = newPlateauCounter
					plateaus[newPlateauCounter] = {}
					table.insert(plateaus[newPlateauCounter], x .. "," .. z)
					newPlateauCounter = newPlateauCounter + 1
				end

				local currentPlateauIndex = pointsToPlateaus[x .. "," .. z]

				if maxHeightMask[(x+step) .. "," .. (z-step)] == 1 and pointsToPlateaus[(x+step) .. "," .. (z-step)] == nil then
					table.insert(plateaus[currentPlateauIndex], (x+step) .. "," .. (z-step))
					pointsToPlateaus[(x+step) .. "," .. (z-step)] = currentPlateauIndex
				end

				if maxHeightMask[(x+step) .. "," .. z] == 1 and pointsToPlateaus[(x+step) .. "," .. z] == nil then
					table.insert(plateaus[currentPlateauIndex], (x+step) .. "," .. z)
					pointsToPlateaus[(x+step) .. "," .. z] = currentPlateauIndex
				end

				if maxHeightMask[(x+step) .. "," .. (z+step)] == 1 and pointsToPlateaus[(x+step) .. "," .. (z+step)] == nil then
					table.insert(plateaus[currentPlateauIndex], (x+step) .. "," .. (z+step))
					pointsToPlateaus[(x+step) .. "," .. (z+step)] = currentPlateauIndex
				end

				if maxHeightMask[x .. "," .. (z+step)] == 1 and pointsToPlateaus[x .. "," .. (z+step)] == nil then
					table.insert(plateaus[currentPlateauIndex], x .. "," .. (z+step))
					pointsToPlateaus[x .. "," .. (z+step)] = currentPlateauIndex
				end
			end
		end
	end

	local function Split(str, delimiter)
		local result = {};
		for match in string.gmatch(str, "([^"..delimiter.."]+)") do
			table.insert(result, match);
		end
		return result;
	end

	local resultPoints = {}
	for i = 1, #plateaus do
		local plateau = plateaus[i]
		table.insert(resultPoints, { x = 0, z = 0})
		-- computing the result point by iterative / online mean of points in plateau
		for n = 1, #plateau do
			local stringXZ = Split(plateau[n], ",")
			local x, z = tonumber(stringXZ[1]), tonumber(stringXZ[2])

			resultPoints[#resultPoints].x = resultPoints[#resultPoints].x + 1/n * (x - resultPoints[#resultPoints].x)
			resultPoints[#resultPoints].z = resultPoints[#resultPoints].z + 1/n * (z - resultPoints[#resultPoints].z)
			n = n + 1
		end
	end

	return resultPoints
end