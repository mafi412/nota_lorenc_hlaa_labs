local sensorInfo = {
	name = "LeaderTooAhead",
	desc = "Returns true, if the leader is too far from its allies.",
	author = "Matyas",
	date = "2022-05-04",
	license = "notAlicense",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description Returns true, if the leader is too far from its allies.
return function(maxDistanceAhead)
	local leaderID = nil
	if #units > 0 then
		for i = 1, #units do
			if Spring.ValidUnitID(units[i]) then
				local defID = Spring.GetUnitDefID(units[i])

				if UnitDefs[defID].name == "armbcom" then
					leaderID = units[i]
					break
				end
			end
		end
	end

	local x1,y1,z1 = Spring.GetUnitPosition(Spring.GetUnitNearestAlly(leaderID))
	local x2,y2,z2 = Spring.GetUnitPosition(leaderID)
	local distance = math.sqrt((x1 - x2)^2 + (z1 - z2)^2)
	return distance > maxDistanceAhead
end