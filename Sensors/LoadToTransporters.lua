local sensorInfo = {
	name = "LoadToTransporters",
	desc = "Sends the given units to load onto the given transporters (while trying to use the given transports equally).",
	author = "Matyas",
	date = "2022-05-11",
	license = "notAlicense",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description Sends the given units to load onto the given transporters (while trying to use the given transports equally).
return function(transportersIDs)
	if #units > 0 then
		for i = 1, #units do
			if Spring.ValidUnitID(units[i]) then
				Spring.GiveOrderToUnit(units[i], CMD.LOAD_ONTO, {transportersIDs[((i-1) % #transportersIDs) + 1]}, {})
			end
		end
	end

	return nil
end