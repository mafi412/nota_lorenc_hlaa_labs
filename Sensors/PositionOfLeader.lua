local sensorInfo = {
	name = "PositionOfLeader",
	desc = "Gets position of leader (battle commander).",
	author = "Matyas",
	date = "2022-05-04",
	license = "notAlicense",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description Gets position of leader (battle commander).
return function()
	if #units > 0 then
		for i = 1, #units do
			if Spring.ValidUnitID(units[i]) then
				local defID = Spring.GetUnitDefID(units[i])

				if UnitDefs[defID].name == "armbcom" then
					x, y, z = Spring.GetUnitPosition(units[i])
					return Vec3(x,y,z)
				end
			end
		end
	end

	return nil
end