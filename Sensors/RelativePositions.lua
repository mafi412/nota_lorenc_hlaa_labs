local sensorInfo = {
	name = "RelativePositions",
	desc = "Returns relative positions.",
	author = "Matyas",
	date = "2022-04-23",
	license = "notAlicense",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description Returns relative positions.
return function()
	local relativePositions = {}
	if #units > 0 then
		local x, y, z = Spring.GetUnitPosition(units[1])
		local position_of_first = { x = x, y = 0, z = z}
		relativePositions[1] = { x = 0, y = 0, z = 0}

		for i = 2, #units do
			if Spring.ValidUnitID(units[i]) then
				local x, y, z = Spring.GetUnitPosition(units[i])
				relativePositions[i] = { x = x - position_of_first.x, y = 0, z = z - position_of_first.z}
			end
		end
	end

	return relativePositions
end