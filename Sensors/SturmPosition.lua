local sensorInfo = {
	name = "SturmPosition",
	desc = "Sturms the given coordinates by all forces available.",
	author = "Matyas",
	date = "2022-05-11",
	license = "notAlicense",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description Sturms the given coordinates by all forces available.
return function(x, z)
	if #units > 0 then
		Spring.GiveOrderToUnitArray(units, CMD.FIGHT, {x, Spring.GetGroundHeight(x, z), z}, {})
	end

	return nil
end