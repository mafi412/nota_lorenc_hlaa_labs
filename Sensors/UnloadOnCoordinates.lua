local sensorInfo = {
	name = "UnloadOnCoordinates",
	desc = "Sends given transport Bear to unload onto the given coordinates x, z.",
	author = "Matyas",
	date = "2022-05-11",
	license = "notAlicense",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description Sends given transport to unload onto the given coordinates x, z.
return function(x, z)
	if #units > 0 then
		for i = 1, #units do
			if Spring.ValidUnitID(units[i]) then
				local defID = Spring.GetUnitDefID(units[i])

				if UnitDefs[defID].name == "armthovr" then
					Spring.GiveOrderToUnit(units[i], CMD.UNLOAD_UNITS, {x, Spring.GetGroundHeight(x,z), z, 100}, {})
				end
			end
		end
	end

	return nil
end